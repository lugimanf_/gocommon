package response

import (
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/lugimanf_/gocommon/utils"
)

//status success
const StatusSuccess = "success"

//status notfound
const StatusUserNotFound = "user.notfound"
const StatusAccountSavingNotFound = "accountsaving.notfound"
const StatusForbiddenPage = "page.forbidden"
const StatusCustomerNotFound = "customer.notfound"
const StatusTransferNotFound = "transfer.notfound"

//status other
const StatusErrorUsernameOrPasswordWrong = "auth.wrong"
const StatusUnauthorized = "auth.unauthorized"
const StatusErrorInternalServer = "apps.internalError"

const TextErrorConfirmPassword = "password and confirm password must same"
const TextErrorUnauthorized = "please login first"

func CodeErrorGenerator(statusError string) *CodedError {
	MinLengthPassword := utils.GetMinLengthPassword()
	MinLengthPhone := utils.GetMinLengthPhone()
	MinTransfer := utils.GetMinTransfer()
	data := CodedError{
		Message:   "",
		ErrorCode: 0,
		ErrorKey:  statusError,
	}
	if statusError == StatusSuccess {
		return &data
	}
	s := strings.Split(statusError, ".")
	switch s[1] {
	case "notfound":
		data.ErrorCode = http.StatusNotFound
		data.Message = fmt.Sprintf("%v not found", s[0])
	case "required":
		switch s[0] {
		case "phone":
			data.ErrorCode = 1001
		case "username":
			data.ErrorCode = 1002
		case "email":
			data.ErrorCode = 1003
		case "password":
			data.ErrorCode = 1005
		case "confirmpassword":
			data.ErrorCode = 1006
		case "agent":
			data.ErrorCode = 1007
		case "accountsavingsender":
			data.ErrorCode = 1008
		case "accountsavingreceiver":
			data.ErrorCode = 1009
		case "amount":
			data.ErrorCode = 1010
		}
		data.Message = fmt.Sprintf("please insert %v", s[0])
	case "email":
		switch s[0] {
		case "email":
			data.ErrorCode = 4001
		}
		data.Message = fmt.Sprintf("format email in field %v is wrong", s[0])
	case "passwd":
		switch s[0] {
		case "password":
			data.ErrorCode = 6001
		}
		data.Message = fmt.Sprintf("password lenght min %v", MinLengthPassword)
	case "phone":
		switch s[0] {
		case "phone":
			data.ErrorCode = 5001
		}
		data.Message = fmt.Sprintf("insert phone number with minimal length %v and use number only", MinLengthPhone)
	case "mintransfer":
		data.ErrorCode = 15100001
		data.Message = fmt.Sprintf("minimal transfer is %v", MinTransfer)
	case "insufficient":
		data.ErrorCode = 11100001
		data.Message = fmt.Sprintf("insufficient balance")
	case "exists":
		switch s[0] {
		case "phone":
			data.ErrorCode = 3001
		case "username":
			data.ErrorCode = 3002
		case "email":
			data.ErrorCode = 3003
		}
		data.Message = fmt.Sprintf("%v exists", s[0])
	case "forbidden":
		data.ErrorCode = http.StatusForbidden
		data.Message = fmt.Sprintf("%v forbidden to use", s[0])
	case "sameaccountsaving":
		data.ErrorCode = 151001
		data.Message = "account saving sender can't same with account saving receiver"
	case "confirmpassword":
		switch s[0] {
		case "confirmpassword":
			data.ErrorCode = 2001
		}
		data.Message = TextErrorConfirmPassword
	default:
		data.Message = fmt.Sprintf("there are field no valid, call administrator immediately")
	}

	if statusError == StatusUnauthorized {
		data.ErrorCode = http.StatusUnauthorized
		data.Message = "username and password not match"
	}

	if statusError == StatusForbiddenPage {
		data.ErrorCode = http.StatusForbidden
		data.Message = "forbidden page"
	}

	if statusError == StatusErrorUsernameOrPasswordWrong {
		data.Message = "username and password not match"
		data.ErrorCode = 130001
	}

	if statusError == StatusErrorInternalServer {
		data.Message = ""
		data.ErrorCode = http.StatusInternalServerError
	}
	return &data
}
