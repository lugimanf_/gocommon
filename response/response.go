package response

import (
	"encoding/json"
	"net/http"
	"time"

	"bitbucket.org/lugimanf_/gocommon/logging"
	"github.com/unrolled/render"
)

const UnexpectedError = "unexpected error please call administrator"

type JsonAPIResponse struct {
	//StatusCode int         `json:"status_code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type ParameterResponse struct {
	Status  int
	Message string
	Data    interface{}
}

type CodedError struct {
	ErrorCode int64  `json:"error_code"`
	Message   string `json:"message"`
	ErrorKey  string `json:"error_key"`
}

func Response(w http.ResponseWriter, data ParameterResponse) error {
	rdr := render.New()
	responseData := JsonAPIResponse{
		//StatusCode: data.Status,
		Message: data.Message,
		Data:    data.Data,
	}
	if data.Status >= 500 {
		responseData.Message = UnexpectedError
	}
	resMarshal, _ := json.Marshal(responseData)
	diff := time.Since(logging.DataLogging.TimeStart).Seconds()
	logging.DataLogging.StatusCode = data.Status
	logging.DataLogging.ExecTime = diff
	logging.DataLogging.Response = string(resMarshal)
	logging.DataLogging.PrintLog()
	return rdr.JSON(w, data.Status, responseData)
}

func SetParameterResponse(errCode *CodedError, res *ParameterResponse) {
	if errCode.ErrorCode == http.StatusInternalServerError {
		res.Status = http.StatusInternalServerError
	} else if errCode.ErrorCode == http.StatusInternalServerError {
		res.Status = http.StatusInternalServerError
	} else if errCode.ErrorCode == http.StatusNotFound {
		res.Status = http.StatusNotFound
		res.Message = errCode.Message
		res.Data = nil
	} else if errCode.ErrorCode == http.StatusForbidden {
		res.Status = http.StatusForbidden
		res.Message = "Forbidden page"
		res.Data = nil
	} else {
		res.Status = http.StatusBadRequest
		res.Message = errCode.Message
	}
}
