package jwt

import (
	"github.com/dgrijalva/jwt-go"
	"os"
	"strconv"
	"time"
)

type JwtGenerator interface {
	CreateToken(payload map[string]interface{}) (string, error)
}

type JwtLogin struct {
	UserID string `json:"user_id"`
	Agent  string `json:"agent"`
}

type Jwt struct{}

func NewJwtGenerator() JwtGenerator {
	return &Jwt{}
}

func (a *Jwt) CreateToken(payload map[string]interface{}) (string, error) {
	payLoadJwt := jwt.MapClaims{}
	for key, val := range payload {
		payLoadJwt[key] = val
	}
	valDuration, _ := strconv.Atoi(os.Getenv("DURATION"))
	duration := time.Duration(1)
	if os.Getenv("TYPE_DURATION") == "HOUR" {
		duration = time.Hour * time.Duration(valDuration)
	} else if os.Getenv("TYPE_DURATION") == "MINUTE" {
		duration = time.Hour * time.Duration(valDuration)
	} else {
		duration = time.Second * time.Duration(valDuration)
	}

	payLoadJwt["exp"] = time.Now().Add(duration).Unix()
	sign := jwt.NewWithClaims(jwt.SigningMethodHS256, payLoadJwt)
	return sign.SignedString([]byte(os.Getenv("JWT_KEY")))
}
