package usecases

const StatusErrorValidation = 1 //status code for error validation payload (400)
const StatusDone = 0            //status code where all process done (200)
const StatusInternalError = 4   //status code where there are unexpected error
const StatusUnauthorized = 3    //status code where request for user doesn't have permission
const StatusNotFound = 2        //status code where not found requested data by user
