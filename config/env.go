package config

import (
	"flag"
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"os"
	"path/filepath"
)

func SetupConfigEnv() {
	var BasePath, _ = os.Getwd()
	EnvLocal := flag.Bool("env-local", false, "display colorized output")
	EnvDevelopment := flag.Bool("env-development", false, "display colorized output")
	EnvStaging := flag.Bool("env-staging", false, "display colorized output")
	flag.Parse()
	fileName := "master.env"
	if *EnvLocal {
		fileName = "local.env"
		log.Println("Using file ", fileName)
	}
	if *EnvDevelopment {
		fileName = "development.env"
		log.Println("Using file ", fileName)
	}
	if *EnvStaging {
		fileName = "staging.env"
		log.Println("Using file ", fileName)
	}
	PathFileEnv := filepath.Join(BasePath+"/environment/", fileName)
	if _, err := os.Stat(PathFileEnv); !os.IsNotExist(err) {
		if err := godotenv.Load(PathFileEnv); err != nil {
			fmt.Println("PathFileEnv:" + PathFileEnv)
			panic("Error loading " + fileName + " file")
		}
	} else {
		panic("File " + PathFileEnv + " not found")
	}
}
