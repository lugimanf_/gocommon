package mq

//
//import (
//	"github.com/ThreeDotsLabs/watermill"
//	"github.com/ThreeDotsLabs/watermill-amqp/pkg/amqp"
//	"github.com/ThreeDotsLabs/watermill-googlecloud/pkg/googlecloud"
//	"github.com/ThreeDotsLabs/watermill/message"
//	"context"
//
//)
//
//type MQGoogleCloud struct {
//	amqpURI 	string
//	config 		*googlecloud.Subscriber
//	logger      watermill.LoggerAdapter
//}
//
//
//func NewMQGoogleCloud(ConnectionURI string) MessageQue{
//	return &MQGoogleCloud{
//		amqpURI: ConnectionURI,
//		logger:  watermill.NewStdLogger(false, false)
//	}
//}
//
//func (a MQGoogleCloud) NewSubcriberGoogleCloud(ConnectionURI string, topic string) (<-chan *message.Message, error){
//	subscriber, err := googlecloud.NewSubscriber(
//		googlecloud.SubscriberConfig{
//			// custom function to generate Subscription Name,
//			// there are also predefined TopicSubscriptionName and TopicSubscriptionNameWithSuffix available.
//			GenerateSubscriptionName: func(topic string) string {
//				return "test-sub_" + topic
//			},
//			ProjectID: "test-project",
//		},
//		a.logger,
//	)
//	if err != nil {
//		panic(err)
//	}
//
//	return subscriber.Subscribe(context.Background(), topic)
//}
//
//func (a MQGoogleCloud) NewPublisher(topic string) (*amqp.Publisher, error){
//	return  googlecloud.NewPublisher(googlecloud.PublisherConfig{
//		ProjectID: "test-project",
//	}, a.logger)
//}
