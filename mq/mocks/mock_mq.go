// Code generated by MockGen. DO NOT EDIT.
// Source: mq/mq.go

// Package mock_mq is a generated GoMock package.
package mocks

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockMQPublisher is a mock of MQPublisher interface.
type MockMQPublisher struct {
	ctrl     *gomock.Controller
	recorder *MockMQPublisherMockRecorder
}

// MockMQPublisherMockRecorder is the mock recorder for MockMQPublisher.
type MockMQPublisherMockRecorder struct {
	mock *MockMQPublisher
}

// NewMockMQPublisher creates a new mock instance.
func NewMockMQPublisher(ctrl *gomock.Controller) *MockMQPublisher {
	mock := &MockMQPublisher{ctrl: ctrl}
	mock.recorder = &MockMQPublisherMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockMQPublisher) EXPECT() *MockMQPublisherMockRecorder {
	return m.recorder
}

// Publish mocks base method.
func (m *MockMQPublisher) Publish(topic string, command interface{}) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Publish", topic, command)
	ret0, _ := ret[0].(error)
	return ret0
}

// Publish indicates an expected call of Publish.
func (mr *MockMQPublisherMockRecorder) Publish(topic, command interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Publish", reflect.TypeOf((*MockMQPublisher)(nil).Publish), topic, command)
}
