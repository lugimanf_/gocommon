package mq

import (
	"context"
	"encoding/json"
	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-amqp/v2/pkg/amqp"
	"github.com/ThreeDotsLabs/watermill/message"
)

type PublisherRabbiMQ struct {
	logger    watermill.LoggerAdapter
	amqpURI   string
	config    amqp.Config
	publisher *amqp.Publisher
}

func NewSubcriberRabbiMQ(ConnectionURI string, topic string) (<-chan *message.Message, error) {
	subscriber, err := amqp.NewSubscriber(
		createConnection(ConnectionURI),
		watermill.NewStdLogger(false, false),
	)
	if err != nil {
		return nil, err
	}

	return subscriber.Subscribe(context.Background(), topic)
}

func NewPublisherRabbiMQ(ConnectionURI string) MQPublisher {
	rabbimq := PublisherRabbiMQ{
		amqpURI: ConnectionURI,
		logger:  watermill.NewStdLogger(false, false),
	}
	publisher, err := amqp.NewPublisher(createConnection(rabbimq.amqpURI), rabbimq.logger)
	if err != nil {
		panic("Create Publisher MQ Error: " + err.Error())
	}
	rabbimq.publisher = publisher
	return &rabbimq

}

func (a PublisherRabbiMQ) Publish(topic string, command interface{}) error {
	res, err := json.Marshal(command)
	if err != nil {
		return err
	}
	msg := message.NewMessage(watermill.NewUUID(), res)
	return a.publisher.Publish(topic, msg)
}

func createConnection(ConnectionURI string) amqp.Config {
	return amqp.NewDurableQueueConfig(ConnectionURI)
}
