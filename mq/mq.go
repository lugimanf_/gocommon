package mq

type MQPublisher interface {
	Publish(topic string, command interface{}) error
}
