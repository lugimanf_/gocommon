package ctx

import (
	"bitbucket.org/lugimanf_/gocommon/jwt"
	"context"
)

func GetAuth(ctx context.Context) jwt.JwtLogin {
	return ctx.Value("auth").(jwt.JwtLogin)
}
