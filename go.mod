module bitbucket.org/lugimanf_/gocommon

go 1.16

require (
	github.com/ThreeDotsLabs/watermill v1.2.0-rc.7
	github.com/ThreeDotsLabs/watermill-amqp/v2 v2.0.3
	github.com/ThreeDotsLabs/watermill-googlecloud v1.0.11 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-pg/pg/v10 v10.10.6
	github.com/golang/mock v1.6.0
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.4.0
	github.com/kr/pretty v0.3.0 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.17.0 // indirect
	github.com/prometheus/client_golang v1.11.0
	github.com/rogpeppe/go-internal v1.8.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/unrolled/render v1.4.1
	go.mongodb.org/mongo-driver v1.8.2
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
