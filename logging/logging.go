package logging

import (
	"bytes"

	"net/http"
	"net/http/httputil"
	"os"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

// UTCFormatter ...
type UTCFormatter struct {
	logrus.Formatter
}

// Data is data standard output
type Data struct {
	RequestID     string            `json:"RequestID"`
	TimeStart     time.Time         `json:"TimeStart"`
	UserID        string            `json:"UserID"`
	Agent         string            `json:"Agent"`
	Service       string            `json:"Service"`
	Host          string            `json:"Host"`
	Endpoint      string            `json:"Endpoint"`
	RequestMethod string            `json:"RequestMethod"`
	RequestHeader string            `json:"RequestHeader"`
	StatusCode    int               `json:"StatusCode"`
	Response      string            `json:"Response"`
	ExecTime      float64           `json:"ExecutionTime"`
	SaltKey       map[string]string `json:"SaltKey"`
	Messages      []string          `json:"Messages"`
	ThirdParty    []ThirdParty      `json:"3rdParty"`
	Route         interface{}       `json:"Route"`
}

// ThirdParty is data logging for any request to outside
type ThirdParty struct {
	RequestID     string `json:"request_id,omitempty"`
	Service       string `json:"service"`
	URL           string `json:"url"`
	RequestHeader string `json:"request_header"`
	RequestBody   string `json:"request_body"`
	Response      string `json:"response"`
	StatusCode    int    `json:"status_code"`
}

var DataLogging *Data

func NewLogging(r *http.Request, dateTimeStart time.Time) {
	SaltKey := make(map[string]string)
	SaltKey["AES256_KEY"] = os.Getenv("AES256_KEY")
	SaltKey["JWT_KEY"] = os.Getenv("JWT_KEY")

	Data := Data{
		RequestID:     uuid.New().String(),
		Service:       os.Getenv("SERVICE_NAME"),
		Host:          r.Host,
		Endpoint:      r.URL.Path,
		TimeStart:     dateTimeStart,
		RequestMethod: r.Method,
		RequestHeader: DumpRequest(r),
		SaltKey:       SaltKey,
		Messages:      []string{},
		ThirdParty:    []ThirdParty{},
	}
	DataLogging = &Data
}

// DumpRequest is for get all data request header
func DumpRequest(req *http.Request) string {
	header, err := httputil.DumpRequest(req, true)
	if err != nil {
		return "cannot dump request"
	}

	trim := bytes.ReplaceAll(header, []byte("\r\n"), []byte("   "))
	return string(trim)
}

func GetDataLogging() Data {
	return *DataLogging
}

func (d *Data) AddMessage(message string) {
	d.Messages = append(d.Messages, message)
}

func (d *Data) Print(message string) {
	if os.Getenv("ENV") != "production" {
		logrus.Println(message)
	}
}

func (d *Data) PrintLog() {
	logrus.SetFormatter(UTCFormatter{&logrus.JSONFormatter{}})
	if d.StatusCode >= 200 && d.StatusCode < 400 {
		logrus.WithField("data", d).Info("apps")
	} else if d.StatusCode >= 400 && d.StatusCode < 500 {
		logrus.WithField("data", d).Warn("apps")
	} else {
		logrus.WithField("data", d).Error("apps")
	}
}
