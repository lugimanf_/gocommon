package logging

import (
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"os"
	"time"
)

type LoggingSubscriber interface {
	SetStatus(status int) LoggingSubscriber
	SetPayload(payload string) LoggingSubscriber
	AddMessage(message string) LoggingSubscriber
	Print(message string)
	PrintLog()

}

type DataLoggingSubscriber struct {
	RequestID  string            `json:"RequestID"`
	TimeStart  time.Time         `json:"TimeStart"`
	ExecTime   float64           `json:"ExecutionTime"`
	UserID     string            `json:"UserID"`
	Service    string            `json:"Service"`
	Topic      string            `json:"Topic"`
	Payload    string            `json:"Payload"`
	StatusCode int               `json:"StatusCode"`
	SaltKey    map[string]string `json:"SaltKey"`
	Messages   []string          `json:"Messages"`
	ThirdParty []ThirdParty      `json:"3rdParty"`
}

func NewLoggingSubscriber(topic string) LoggingSubscriber{
	SaltKey := make(map[string]string)
	SaltKey["AES256_KEY"] = os.Getenv("AES256_KEY")
	SaltKey["JWT_KEY"] = os.Getenv("JWT_KEY")
	dateTimeStart := time.Now()
	data := DataLoggingSubscriber{
		RequestID:  uuid.New().String(),
		Service:    os.Getenv("SERVICE_NAME"),
		Topic:      topic,
		TimeStart:  dateTimeStart,
		SaltKey:    SaltKey,
		Messages:   []string{},
		ThirdParty: []ThirdParty{},
	}
	return &data
}

func (d *DataLoggingSubscriber) Print(message string) {
	if os.Getenv("ENV") != "production" {
		logrus.Println(message)
	}
}

func (d *DataLoggingSubscriber) AddMessage(msg string) LoggingSubscriber{
	d.Messages = append(d.Messages, msg)
	return d
}

func (d *DataLoggingSubscriber) SetStatus(status int) LoggingSubscriber{
	d.StatusCode = status
	return d
}

func (d *DataLoggingSubscriber) SetPayload(payload string) LoggingSubscriber{
	d.Payload = payload
	return d
}

func (d *DataLoggingSubscriber) PrintLog() {
	d.ExecTime = time.Since(d.TimeStart).Seconds()
	logrus.SetFormatter(UTCFormatter{&logrus.JSONFormatter{}})
	if d.StatusCode >= 200 && d.StatusCode < 400 {
		logrus.WithField("data", d).Info("apps")
	} else if d.StatusCode >= 400 && d.StatusCode < 500 {
		logrus.WithField("data", d).Warn("apps")
	} else {
		logrus.WithField("data", d).Error("apps")
	}
}
