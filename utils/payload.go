package utils

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
)

func GetPayloadFromBodyBase64(r *http.Request, data interface{}) error {
	var sDecode []byte
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return errors.New("(ioutil) " + err.Error())
	}
	result, err := strconv.Unquote(string(body))
	if err != nil {
		sDecode, err = base64.StdEncoding.DecodeString(string(body))
	} else {
		sDecode, err = base64.StdEncoding.DecodeString(result)
	}
	if err != nil {
		return errors.New("(DecodeString) " + err.Error())
	}

	err = json.Unmarshal(sDecode, &data)
	if err != nil {
		return errors.New("(Unmarshal) " + err.Error())
	}

	return nil
}
