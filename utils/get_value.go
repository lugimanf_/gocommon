package utils

import (
	"os"
	"strconv"
)

func GetMinLengthPassword() int{
	MinLengthPassword, err := strconv.Atoi(os.Getenv("MIN_LENGTH_PASSWORD"))
	if err != nil  || MinLengthPassword == 0{
		MinLengthPassword = 8
	}

	return MinLengthPassword
}

func GetMinLengthPhone() int{
	MinLengthPhone, err := strconv.Atoi(os.Getenv("MIN_LENGTH_PHONE"))
	if err != nil  || MinLengthPhone == 0{
		MinLengthPhone = 8
	}

	return MinLengthPhone
}

func GetMinTransfer() int{
	MinTransfer, err := strconv.Atoi(os.Getenv("MIN_TRANSFER"))
	if err != nil  || MinTransfer == 0{
		MinTransfer = 10000
	}

	return MinTransfer
}