package utils

import (
	"encoding/json"
	"go.mongodb.org/mongo-driver/bson"
)

func StructToMap(data interface{}) map[string]interface{} {
	var inInterface map[string]interface{}
	inrec, _ := json.Marshal(data)
	json.Unmarshal(inrec, &inInterface)
	return inInterface
}

func SetBsonFromStruct(data interface{}) bson.M {
	dataSet := bson.M{}
	for key, val := range StructToMap(data) {
		if val != nil {
			dataSet[key] = val
		}
	}
	return dataSet
}
