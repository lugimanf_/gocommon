package middlewares

import (
	"bitbucket.org/lugimanf_/gocommon/jwt"
	"bitbucket.org/lugimanf_/gocommon/logging"
	"bitbucket.org/lugimanf_/gocommon/response"
	"context"
	"encoding/json"
	jwt2 "github.com/dgrijalva/jwt-go"
	"net/http"
)

func MiddlewareAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		jwtAuth := jwt.JwtLogin{}
		token, err := jwt.VerifyToken(r)
		if err != nil {
			response.Response(w, response.ParameterResponse{
				Status:  http.StatusUnauthorized,
				Message: response.TextErrorUnauthorized,
			})
			return
		}
		claims, _ := token.Claims.(jwt2.MapClaims)
		result, _ := json.Marshal(claims)
		_ = json.Unmarshal(result, &jwtAuth)
		logging.DataLogging.Agent = jwtAuth.Agent
		logging.DataLogging.UserID = jwtAuth.UserID
		ctx = context.WithValue(ctx, "auth", jwtAuth)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
