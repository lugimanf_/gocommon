package database

import (
	"errors"
)

var (
	ErrorDataNotFound = errors.New("data not found")
	ErrorConnection = errors.New("disconnect server database")
	ErrorUnexpected = errors.New("unexpected error")
)
